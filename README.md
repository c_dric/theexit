In this document you will find info on each script.
What it does and how it is used.
Let me know if something is not clear.

1 / Namespace init
==================

This script is only executed once (by me) and then removed.
It allows me to set up the namespace, the functions and the persistent variables used in The Exit.
All variables are relative to the player, except for unique_visitors which is shared by all players.

2 / Intro area
==============

This is an area script bound to the start room.
It displays a title, a subtitle and particles (if those are enabled in the client).
Along with some ambient sounds.

It only plays the first time a player enters the area.
As it only plays once per player, it is also used to increment a global var : unique_visitors.

3 / Start plate
===============

This interact script is bound to a plate in the start room.
It will set (or reset) the variables at start (or restart).
It is also in this script that the content of the loot boxes is assigned.
A random number is picked and a function box_items is called to assign an item for each loot box.

As all those variables are relative to the player,
players playing at the same time will find different items in the same box.
Also a player starting or restarting will have no effect on players already in the map.

4 / Function box_items
======================

This function is called by the interact script on the start plate and assigns an item for each loot box.

5 / Box function calls
======================

Those 3 lines of interact script are bound to each of the 6 loot boxes.

As the code in each box is 99% identical, it has been moved to a function : box_interact
Each box calls that function with 3 parameters : box_interact(Int lock, Int item, Player box_user)

6 / Function box_interact
=========================

Function called by the 6 loot boxes (as explained in 5 / Box function calls)

Box 1 will call box_interact(box1_lock, box1_item, box_user),
box 2 will call box_interact(box2_lock, box2_item, box_user) and so on...

The lock is 1 or 0 if the player has already found that box or not.
The item is the item assigned for that box and for that player at (re)start.
The user is the player who will received the item if he hasn't yet (lock == 0).
Then the box is locked for that player (lock == 1) until next restart.

7 / Function box_effects
========================

Function called by an area script at each loot boxes.
It is locked when the item has already been looted.

8 / End plate
=============

This interact script is bound on a plate before the exit.

It will make a sum of the lock values of the 6 boxes for that player.
If the sum is less than 4, meaning the player has found less than 4 boxes,
it will tell the player that it won't be enough to survive the exit.
Else, the exit sequence will start. The gates will open, a bridge will appear
and the arrow turrets will become active.
